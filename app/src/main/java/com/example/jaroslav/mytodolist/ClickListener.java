package com.example.jaroslav.mytodolist;

import android.view.View;

/**
 * Created by JAROSLAV on 15.03.2018.
 */

public interface ClickListener {
    public void onClick(View view,int position);
    public void onLongClick(View view,int position);
}
