package com.example.jaroslav.mytodolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by JAROSLAV on 13.03.2018.
 */
//class for communication activity and recycler view
public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.ViewHolder> {
    public List<ToDo> toDoList;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, text, time;
        public RelativeLayout viewBackground, viewForeground;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            title = (TextView) v.findViewById(R.id.title);
            text = (TextView) v.findViewById(R.id.text);
            time = (TextView) v.findViewById(R.id.date);
            viewBackground = (RelativeLayout) v.findViewById(R.id.backgroundView);
            viewForeground = (RelativeLayout) v.findViewById(R.id.foregroundView);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public ToDoListAdapter(List<ToDo> _toDoList) {

        toDoList = _toDoList;
        notifyItemRangeChanged(0, _toDoList.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_text_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ToDo item = toDoList.get(position);
        holder.title.setText(item.getTitle());
        holder.text.setText(item.getText());
        holder.time.setText(item.getTime());
    }

    @Override
    public int getItemCount() {
        return toDoList.size();
    }

    public void removeAt(int position) {
        toDoList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, toDoList.size());
    }

    public void restoreItem(ToDo item, int position) {
        toDoList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
    public void AddNewItem(ToDo item){
        int insertInd=toDoList.size();
        if(MainActivity.sortOrder== MainActivity.Order.byTitle) {
            for (int i=0;i<toDoList.size();i++)
                if(toDoList.get(i).getTitle().compareTo(item.getTitle())>=0)
                { insertInd=i;
                break;}
        }
        toDoList.add(insertInd,item);
        notifyItemInserted(insertInd);
    }
    public void reloadItems(List<ToDo> items){
        toDoList.clear();
        toDoList.addAll(items);
        notifyItemRangeInserted(0,items.size());
    }
    public void editItem(int possition,ToDo item){
        toDoList.remove(possition);
        int insertInd=toDoList.size();
        if(MainActivity.sortOrder== MainActivity.Order.byTitle) {
            for (int i=0;i<toDoList.size();i++)
                if(toDoList.get(i).getTitle().compareTo(item.getTitle())>=0)
                { insertInd=i;
                    break;}
        }
        else{
            for (int i=0;i<toDoList.size();i++)
                if(toDoList.get(i).getTime().compareTo(item.getTime())>=0)
                { insertInd=i;
                    break;}
        }
        if(insertInd!=possition) {
            notifyItemRemoved(possition);
            toDoList.add(insertInd, item);
            notifyItemInserted(insertInd);
            return;
        }
        toDoList.add(possition, item);
        notifyItemChanged(possition);
    }
}
