package com.example.jaroslav.mytodolist;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements SwipeToDeleteCallback.RecyclerItemTouchHelperListener {
    private RecyclerView mRecycleView;
    private ToDoListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RelativeLayout mLayout;
    SettingFragment settingFragment;
    private List<ToDo> toDoList = new ArrayList<>();

    public enum Order{
        byDate,
        byTitle
    };
    static Order sortOrder=Order.byDate;
    static String title="MyToDoApp";
    public static final int ADD_REQUEST = 1;
    public static final int EDIT_REQUEST = 2;

    DBHelper dbHelper;

    private int editItemPossition=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLayout =(RelativeLayout)findViewById(R.id.mLinearLayout);
        mRecycleView = (RecyclerView) findViewById(R.id.my_recycler_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        title = prefs.getString("appTitle","");
        sortOrder=Order.values()[Integer.parseInt(prefs.getString("sortType",null))-1];
        setActionBarTitle(title);

        Toolbar toolbar=(Toolbar)findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        dbHelper=new DBHelper(this);
        toDoList.addAll(dbHelper.getAllItems(sortOrder));

        mLayoutManager = new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(mLayoutManager);

        mRecycleView.setItemAnimator(new DefaultItemAnimator());//add default animation
        mRecycleView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));//add vertical dividing line

        mAdapter = new ToDoListAdapter(toDoList);
        mRecycleView.setAdapter(mAdapter);
        final Activity act=this;
        //on recycler view element long and short click
        mRecycleView.addOnItemTouchListener(new RecycleTouchListener(this, mRecycleView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Toast.makeText(MainActivity.this, "Single Click on position        :" + position,
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                editItemPossition=position;
                Intent intent=new Intent(act,AddItemActivity.class);
                intent.putExtra("title",toDoList.get(position).getTitle());
                intent.putExtra("text",toDoList.get(position).getText());
                startActivityForResult(intent,EDIT_REQUEST);
            }
        }));

        //add swipe item for deleting functionality
        ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new SwipeToDeleteCallback(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(mRecycleView);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    public void onSettingClick(MenuItem v){
        settingFragment = new SettingFragment();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mLinearLayout, settingFragment).addToBackStack(null).commit();
        setTitle("Settings");
    }
    //get back from settigs
    public void onBackClick(MenuItem v){

        if (settingFragment != null) {
            if (settingFragment.getFragmentManager().getBackStackEntryCount() == 0) {
                this.finish();
            } else {
                settingFragment.getFragmentManager().popBackStack();
            }
        }
    }
    public void restoreSortedData(){
        toDoList.clear();
        toDoList.addAll(dbHelper.getAllItems(sortOrder));
        mAdapter.notifyDataSetChanged();
    }
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ToDoListAdapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = toDoList.get(viewHolder.getAdapterPosition()).getTitle();

            // backup of removed item for undo purpose
            final ToDo deletedItem = toDoList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            mAdapter.removeAt(viewHolder.getAdapterPosition());
            //remove from database
            dbHelper.deleteNote(deletedItem);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(mLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //restore in database
                    deletedItem.setId(dbHelper.insertItem(deletedItem));
                    // undo is selected, restore the deleted item
                    mAdapter.restoreItem(deletedItem, deletedIndex);

                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
    //click on add icon starting new activity
    public void addIconClick(View v){
        v.startAnimation(AnimationUtils.loadAnimation(this,R.anim.click));
        Intent intent=new Intent(this,AddItemActivity.class);

        startActivityForResult(intent,ADD_REQUEST);
    }
    //result of finished activity
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_REQUEST && resultCode==RESULT_OK)
        {
            ToDo n_todo=new ToDo();

            n_todo.setText(data.getStringExtra(AddItemActivity.Content_Text));
            String title=data.getStringExtra(AddItemActivity.Content_Title);

            if(title.length()!=0)
               n_todo.setTitle(title);

            //add new item to data base
           long id=dbHelper.insertItem(n_todo);
           n_todo=dbHelper.getItem(id);

           //add new item to the recycler view list
            mAdapter.AddNewItem(n_todo);
        }
        if(requestCode==EDIT_REQUEST && resultCode==RESULT_OK){
            if(editItemPossition==-1)
                return;
            ToDo n_todo=new ToDo();

            n_todo.setText(data.getStringExtra(AddItemActivity.Content_Text));
            String title=data.getStringExtra(AddItemActivity.Content_Title);
            if(title.length()!=0)
                n_todo.setTitle(title);
            n_todo.setId(toDoList.get(editItemPossition).getId());
            n_todo.setTime(toDoList.get(editItemPossition).getTime());
            //add new item to data base
            dbHelper.updateItem(n_todo);
            //add new item to the recycler view list
            mAdapter.editItem(editItemPossition,n_todo);
        }
    }
    public void setActionBarTitle(String title) {
        if(title.equals("Settings"))
        {
            setTitle(title);
             return;
        }
        setTitle(title+ " ToDo List");

    }
}
