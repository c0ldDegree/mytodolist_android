package com.example.jaroslav.mytodolist;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by JAROSLAV on 14.03.2018.
 */
//class where note info is stored
public class ToDo {
    public static final String TABLE_NAME = "toDo";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_TEXT = "content";
    public static final String COLUMN_TIME = "time";

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +COLUMN_TITLE+" TEXT,"
                    + COLUMN_TEXT + " TEXT,"
                    + COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";

    private long id;
    private String text,title,time;
    public ToDo(){
        title="[No title]";
        text="";
       // DateFormat dateFormat = new SimpleDateFormat("dd.MM HH:mm");
      //  Date date=new Date();
      //  time=dateFormat.format(date);
    }

    public ToDo(long id,String text) {
        this.id=id;
        this.text = text;
        title="[No title]";
      //  DateFormat dateFormat = new SimpleDateFormat("dd.MM HH:mm");
      //  Date date=new Date();
     //   time=dateFormat.format(date);
    }

    public ToDo(int id,String text, String title) {
        this.id=id;
        this.text = text;
        this.title = title;
      //  DateFormat dateFormat = new SimpleDateFormat("dd.MM HH:mm");
      //  Date date=new Date();
      //  time=dateFormat.format(date);

    }
    public ToDo(int id, String text, String title, String time) {
        this.id=id;
        this.text = text;
        this.title = title;
        this.time=formatDate(time);

    }
    public void setId(long id) {
        this.id = id;
    }

    public void setTime(String time) {

        this.time =formatDate(time);
        if(this.time.equals(""))
            this.time=time;
    }


    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String getTime() {
        return time;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String formatDate(String dateStr) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateStr);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd.MM HH:mm");
            return fmtOut.format(date);
        } catch (ParseException e) {

        }

        return "";
    }
}
