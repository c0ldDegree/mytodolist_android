package com.example.jaroslav.mytodolist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class AddItemActivity extends AppCompatActivity {
    public static final String Content_Title =
            "com.example.android.toDoList.extra.title";
    public static final String Content_Text =
            "com.example.android.toDoList.extra.text";
    private EditText title;
    private EditText content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        title=(EditText)findViewById(R.id.title);
        content=(EditText)findViewById(R.id.content);
        //new toolbar with accept icon
        Toolbar toolbar=(Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        if(bundle!=null) {
            title.setText(bundle.getString("title"));
            content.setText(bundle.getString("text"));
        }
    }

    //new menu with accept icon
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add,menu);
        return true;
    }
    //return info to main activity
    public void onAddClick(MenuItem item) {
        String title_text=title.getText().toString();
        String text=content.getText().toString();

        Intent intent=new Intent();
        intent.putExtra(Content_Title,title_text);
        intent.putExtra(Content_Text,text);
        setResult(RESULT_OK,intent);
        finish();
    }
}
