package com.example.jaroslav.mytodolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JAROSLAV on 24.03.2018.
 */
//database functionality class
public class DBHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "toDo_db2";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ToDo.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ToDo.TABLE_NAME);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public long insertItem(ToDo item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ToDo.COLUMN_TITLE, item.getTitle());
        values.put(ToDo.COLUMN_TEXT, item.getText());

        long id = db.insert(ToDo.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    public ToDo getItem(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(ToDo.TABLE_NAME,
                new String[]{ToDo.COLUMN_ID, ToDo.COLUMN_TITLE, ToDo.COLUMN_TEXT, ToDo.COLUMN_TIME},
                ToDo.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        ToDo item = new ToDo(
                cursor.getInt(cursor.getColumnIndex(ToDo.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TEXT)),
                cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TIME)));

        // close the db connection
        cursor.close();

        return item;
    }

    public List<ToDo> getAllItems(MainActivity.Order order) {
        List<ToDo> items = new ArrayList<>();

        String selectQuery;
        if(order==MainActivity.Order.byDate) {
            // Select All Query
            selectQuery = "SELECT  * FROM " + ToDo.TABLE_NAME + " ORDER BY " +
                    ToDo.COLUMN_TIME;
        }
        else
            selectQuery="SELECT  * FROM " + ToDo.TABLE_NAME + " ORDER BY " +
                    ToDo.COLUMN_TITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ToDo item = new ToDo();
                item.setId(cursor.getInt(cursor.getColumnIndex(ToDo.COLUMN_ID)));
                item.setTitle(cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TITLE)));
                item.setText(cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TEXT)));
                item.setTime(cursor.getString(cursor.getColumnIndex(ToDo.COLUMN_TIME)));

                items.add(item);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return items;
    }

    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + ToDo.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateItem(ToDo note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ToDo.COLUMN_TEXT, note.getText());
        values.put(ToDo.COLUMN_TITLE, note.getTitle());


        // updating row
        return db.update(ToDo.TABLE_NAME, values, ToDo.COLUMN_ID + " = ?",
                new String[]{String.valueOf(note.getId())});
    }

    public void deleteNote(ToDo item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ToDo.TABLE_NAME, ToDo.COLUMN_ID + " = ?",
                new String[]{String.valueOf(item.getId())});
        db.close();
    }
}
